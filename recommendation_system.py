import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

def calculate_cost(m, hypothesis, y):
    """
    This is a linear regression cost function
    :param m number of data points
    :param hypothesis: simple linear hypothesis
    :param y: actual values
    :return cost
    
    """
    cost = (1/(2*m))*np.sum((hypothesis - y)**2)
    return cost

def generate_hypothesis(theta, X):
    """
    Wrapper for hypothesis function
    :param theta: shape must be compatible with data
    :param X: data
    """
    hypothesis = np.dot(theta.T, np.array([X[:, 0], X[:, 1], X[:, 2], X[:, 3]]))
    #hypothesis = np.dot(theta.T, X.T) # Identically the same if no polynomials are needed
    return hypothesis

def gradient_descent(hypothesis, y, X, theta, alpha):
    """
    No regularisation applied
    We do not regularise for theta0.
    """
    m = X.shape[0]
    grads = (1/m)*np.sum(np.multiply((hypothesis - y), X.T), axis=1)
    theta = theta.T - alpha*grads
    return theta.T

def test_linearRegressin(X, y, alpha, iterations):

    m = X.shape[0]
    n = X.shape[1]
    theta = np.zeros([n, 1])

    cost_array = []
    for i in range(iterations):
        """
        training data: age, height, weight
        target variable: networth
        """
        hypothesis = generate_hypothesis(theta, X)
        cost = calculate_cost(m, hypothesis, y)
        
        theta = gradient_descent(hypothesis, y, X, theta, alpha)

        cost_array.append(cost)

    plt.plot(range(iterations), cost_array)
    print (theta)


# Start Linear Regression with some made up test data:
data_names = ['age', 'height', 'weight', 'netWorth']
data = np.array([[2, 80, 4, 1000], 
                 [5, 100, 5, 2000], 
                 [12, 150, 9, 5000], 
                [13, 155, 10, 5100],
                [14, 158, 10, 5200],
                 [18, 170, 10, 24000], 
                 [23, 172, 11, 28000], 
                 [40, 172, 12, 48000]])

X = np.concatenate([np.ones([8, 1]), data[:, :3]], axis = 1)
y = data[:, 3]


test_linearRegressin(X, y, 0.00001, 100000)




#Collaborative Filtering


movies = np.array(["Notting Hill", "Titanic", "Beauty and the Beast", "Iron Man", "Batman", "Taken", "WW2"])
genreTypes = ["romance", "action", "history"]
users = np.array(["James", "Winson", "Lauren", "Gareth", "Daniel"])
genre = np.array([
    [5, 0, 2], 
    [5, 0, 4], 
    [5, 0, 0],
    [0, 5, 0],
    [0, 5, 0],
    [0, 5, 0],
    [0, 0, 5]
])

ratings = np.array([
    [0, 0, 0, 5, 5, np.nan, np.nan],
   [0, 0, 0, np.nan, 5, 5, np.nan],
   [5, 5, np.nan, 0, 0, 0, np.nan],
    [5, 5, np.nan, 0, 0, 0, np.nan],
    [0, 0, 0, 0, 0, 0, 5]
])


r = np.isnan(ratings)
n_movies = len(movies)
features = np.concatenate([np.ones([n_movies, 1]), genre], axis = 1)

m = features.shape[0]

alpha = 0.00001
theta = np.zeros([ratings.shape[0], features.shape[1]])
iterations = range(10000)
cost_list = []

for i in iterations:

    hypothesis = np.dot(theta, features.T) # (3 people, 6 movies)
    ratings[r] = 0 # all missing data is assigned with 0
    hypothesis[r] = 0 # all hypothesis of missing data is assigned with 0

    cost = (1/(2*m))*np.sum((hypothesis - ratings)**2)
    cost_list.append(cost)

    theta = theta - (alpha * np.dot((hypothesis - ratings), features))


print ("users: {}".format(users))

prediction =  (np.dot(theta, features.T).round(2))
indices = np.argmax(prediction.dot(genre), axis = 1)
map_genres = np.array(genreTypes)[indices]
print (dict(zip(users, map_genres)))

